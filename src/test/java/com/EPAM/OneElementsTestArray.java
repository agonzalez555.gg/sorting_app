package com.EPAM;

import com.EPAM.Aplication.Model.BubbleSort;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

/**
 * Parameterized test class for testing the BubbleSort class with single-element arrays.
 */
@RunWith(Parameterized.class)
public class OneElementsTestArray {

    protected BubbleSort bubble = new BubbleSort();
    private int[] arr;

    public OneElementsTestArray(int[] arr){
        this.arr = arr;
    }

    /**
     * Provides test data consisting of single-element arrays.
     * @return A collection of object arrays, each containing a single-element integer array.
     */
    @Parameterized.Parameters
    public static Collection<Object[]> data(){
        return Arrays.asList(new Object[][]{
                {new int[]{2}},
                {new int[]{3}},
                // ... other test cases ...
        });
    }

    /**
     * Tests the sorting behavior of the BubbleSort class with a single-element array.
     * The test verifies that the array length remains unchanged.
     */
    @Test
    public void oneElementArray(){
        bubble.sortArray(arr);
        assertEquals(1, arr.length);
    }
}
