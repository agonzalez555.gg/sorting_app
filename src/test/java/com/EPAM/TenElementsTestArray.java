package com.EPAM;

import com.EPAM.Aplication.Model.BubbleSort;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.util.Arrays;
import java.util.Collection;

/**
 * Parameterized test class for testing the BubbleSort class with arrays having exactly ten elements.
 */
@RunWith(Parameterized.class)
public class TenElementsTestArray {

    protected BubbleSort bubble = new BubbleSort();
    private int[] arr;

    public TenElementsTestArray(int[] arr){
        this.arr = arr;
    }

    /**
     * Provides test data consisting of arrays with exactly ten elements.
     * @return A collection of object arrays, each containing a ten-element integer array.
     */
    @Parameterized.Parameters
    public static Collection<Object[]> data(){
        return Arrays.asList(new Object[][]{
                {new int[]{10, 20, 30, 40, 50, 60, 70, 100}},
                {new int[]{5, 15, 25, 35, 45, 55, 65, 75, 85, 95, 100, 200, 300}},
                // ... other test cases ...
        });
    }

    /**
     * Tests the behavior of the BubbleSort class when sorting arrays with exactly ten elements.
     * This test is expected to throw IllegalArgumentException.
     */
    @Test(expected = IllegalArgumentException.class)
    public void TenElementArray(){
        bubble.sortArray(arr);
    }
}
