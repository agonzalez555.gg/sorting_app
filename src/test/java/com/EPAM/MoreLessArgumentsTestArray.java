package com.EPAM;

import com.EPAM.Aplication.Model.BubbleSort;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.util.Arrays;
import java.util.Collection;

/**
 * Parameterized test class for testing the BubbleSort class with varying numbers of elements.
 * This class tests the BubbleSort's behavior with arrays having more than ten elements.
 */
@RunWith(Parameterized.class)
public class MoreLessArgumentsTestArray {

    protected BubbleSort bubble = new BubbleSort();
    private int[] arr;

    /**
     * Constructs a new MoreLessArgumentsTestArray instance with the specified array of integers.
     * @param arr The array of integers to be tested.
     */
    public MoreLessArgumentsTestArray(int[] arr){
        this.arr = arr;
    }

    /**
     * Provides test data with arrays of different lengths, including those with more than ten elements.
     * @return A collection of object arrays, each containing an integer array for testing.
     */
    @Parameterized.Parameters
    public static Collection<Object[]> data(){
        return Arrays.asList(new Object[][]{
                {new int[]{1,2,3,4,5,6,7,8,9,10,11}},
                {new int[]{2,2,2,2,2,2,2,2,2,2,2,2}},
                // ... other test cases ...
        });
    }

    /**
     * Tests the behavior of the BubbleSort class when sorting arrays with more than ten elements.
     * This test is expected to throw UnsupportedOperationException.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void MoreThanTenElementsArray(){
        bubble.sortArray(arr);
    }
}
