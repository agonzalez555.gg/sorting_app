package com.EPAM;

import com.EPAM.Aplication.Model.BubbleSort;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

/**
 * Parameterized test class for testing the correct sorting behavior of the BubbleSort class.
 * This class uses different sets of integer arrays to validate that the BubbleSort algorithm
 * sorts an array of integers correctly under various conditions.
 */
@RunWith(Parameterized.class)
public class CorrectSortingTestArray {
    protected BubbleSort bubble = new BubbleSort();

    private int[] arr;

    /**
     * Constructs a new CorrectSortingTestArray instance with the specified array of integers.
     *
     * @param arr The array of integers to be tested for correct sorting.
     */
    public CorrectSortingTestArray(int[] arr){
        this.arr = arr;
    }

    /**
     * Provides different sets of integer arrays to be used as parameters for the correctSortTest.
     * Each array represents a different test case with varying order and values.
     *
     * @return A collection of object arrays, each containing a single integer array for testing.
     */
    @Parameterized.Parameters
    public static Collection<Object[]> data(){
        return Arrays.asList(new Object[][]{
                {new int[]{10, 20, 30, 40, 50, 60, 70, 80, 90, 100}},
                {new int[]{5, 15, 25, 35, 45, 55, 65, 75, 85, 95}},
                {new int[]{1, 11, 21, 31, 41, 51, 61, 71, 81, 91}},
                {new int[]{10, 9, 8, 7, 6, 5, 4, 3, 2, 1}},
                {new int[]{3, 13, 8, 6, 43, 53, 7, 73, 83, 4}}
        });
    }

    /**
     * Tests the sorting of the provided array by the BubbleSort class.
     * This test verifies that the sorted array matches the expected outcome
     * (the array sorted by Java's built-in sort function).
     */
    @Test
    public void correctSortTest(){
        int[] temp = new int[arr.length];
        int counter = 0;
        for(int x:arr){
            temp[counter++] = x;
        }
        Arrays.sort(temp);
        bubble.sortArray(arr);

        assertArrayEquals(arr, temp);
    }
}
