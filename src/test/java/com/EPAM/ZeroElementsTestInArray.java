package com.EPAM;

import com.EPAM.Aplication.Model.BubbleSort;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.util.Arrays;
import java.util.Collection;

/**
 * Parameterized test class for testing the BubbleSort class with empty arrays.
 * This class validates that the BubbleSort algorithm throws an exception for empty arrays.
 */
@RunWith(Parameterized.class)
public class ZeroElementsTestInArray {
    protected BubbleSort bubble = new BubbleSort();
    private int[] arr;

    public ZeroElementsTestInArray(int[] arr){
        this.arr = arr;
    }

    /**
     * Provides test data consisting of empty arrays.
     * @return A collection of object arrays, each containing an empty integer array.
     */
    @Parameterized.Parameters
    public static Collection<Object[]> data(){
        return Arrays.asList(new Object[][]{
                {new int[]{}},
        });
    }

    /**
     * Tests the behavior of the BubbleSort class when sorting an empty array.
     * This test expects an IllegalArgumentException to be thrown.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testEmptyArray(){
        bubble.sortArray(arr);
    }
}
