package com.EPAM.Aplication;

import com.EPAM.Aplication.Model.BubbleSort;
import java.util.Arrays;

/**
 * This is the main class of the application that demonstrates the use of the algorithm bubble sort.
 */
public class App {

    /**
     * Where the main method is.
     * This method initializes an array of integers and sorts it using the bubble sort method
     * provided by the BubbleSort class in the package Model. Then, it prints the sorted array in ascending order to the console.
     *
     * @param args Command line arguments (not used in this application).
     */
    public static void main(String[] args) {
        int[] arrayNumer = new int[]{2, 8, 23, 1, 90, 4, 55, 37,23,55};
        BubbleSort bubble = new BubbleSort();
        bubble.sortArray(arrayNumer);

        for (int x : arrayNumer) {
            System.out.println(x);
        }
    }
}
