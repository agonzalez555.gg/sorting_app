package com.EPAM.Aplication.Model;

/**
 * This class provides a static method for sorting an array of integers using the bubble sort algorithm.
 */
public class BubbleSort {

    /**
     * Sorts an array of integers in ascending order using the bubble sort algorithm.
     * This method iterates over the array, repeatedly swapping adjacent elements if they are in the wrong order.
     * Going one by one and verifying if the current index is smaller than the lesser index.
     *
     * @param arr The array of integers to be sorted. The array will be modified in place.
     */

    public BubbleSort(){

    }
    public void sortArray(int[] arr){
        if(arr.length == 0){
            throw new IllegalArgumentException("Array is Empty");
        }else if(arr.length != 10){
            throw new UnsupportedOperationException("More or less than 10 elements in array, unable to continue.");
        }else {
            for (int i = 0; i < arr.length; i++) {
                for (int j = 1; j < arr.length; j++) {
                    if (arr[j] < arr[j - 1]) {
                        int temp = arr[j - 1];
                        arr[j - 1] = arr[j];
                        arr[j] = temp;
                    }
                }
            }
        }
    }
}